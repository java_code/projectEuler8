/**
 * org.jrt.pe_eight is a list of operations to complete project euler problem eight
 */
package org.jrt.pe_eight;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

/**
 * The four adjacent digits in the 1000-digit number numberGrid.txt that have the greatest product are 9 � 9 � 8 � 9 = 5832.
 * 
 * Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?
 * 
 * @author jrtobac
 *
 */
public class Main {

	public static void main(String[] args) {
		try(BufferedReader br = new BufferedReader(new FileReader("resources\\numberGrid.txt"))) {
			BigInteger[] inNums = new BigInteger[1000];
			BigInteger greatest = new BigInteger("0");
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    //Read in File
		    while (line != null) {
		        sb.append(line);
		        line = br.readLine();
		    }
		    char[] everything = sb.toString().toCharArray();
		    
		    //Convert to BigIntegers
		    for(int i=0; i<everything.length; i++) {
		    	inNums[i] = BigInteger.valueOf(Character.getNumericValue(everything[i]));
		    }
		    
		    //Find greatest product
		    for(int x = 13; x < 1000; x++) {
				BigInteger prod = new BigInteger("1");
				for(int i = 0; i < 13; i++) {
					prod = prod.multiply(inNums[x-i]);
				}
				if(prod.compareTo(greatest) > 0) {
					greatest = prod;
				}
		    }
		    
		    System.out.println(greatest);
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
